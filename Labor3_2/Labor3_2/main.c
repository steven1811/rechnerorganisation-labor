/*
 * Labor3_2.c
 *
 * Assembler 2 - Timing
 * Author : Steven
 */ 
#include "defines.h"

#include <avr/io.h>
#include <stdint.h>
	
extern volatile uint16_t ms;
extern volatile uint8_t ss;
extern volatile uint8_t mm;
extern volatile uint8_t hh;

extern void tick();
extern int func();

int main(void)
{
	int millis;
	int sekunden;
	int minuten;
	int stunden;

	while(1) {
		tick();
		millis = ms;
		sekunden = ss;
		minuten = mm;
		stunden = hh;
	}
}


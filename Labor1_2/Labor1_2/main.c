/*
 * Labor1_2.c
 *
 * Bin�rz�hler
 * Author : Steven
 */ 

#include "defines.h"

#include <avr/io.h>
#include <util/delay.h>

#define DELAY_MS 200
#define INITIAL_VAL 0;

int main(void)
{
	TASTER_DDR	&= ~BIT(TASTER1); // PB0 input
	TASTER_PORT |= BIT(TASTER1); // PB0 with pull-up
	
	LED_DDR_BIT0_2 |= BIT(LED_BIT0) | BIT(LED_BIT1) | BIT(LED_BIT2);
	LED_DDR_BIT3_7 |= BIT(LED_BIT3) | BIT(LED_BIT4) | BIT(LED_BIT5) | BIT(LED_BIT6) | BIT(LED_BIT7);

	uint8_t count=INITIAL_VAL;

	while (1)
	{
		setLEDs(count);
		count++;
		_delay_ms(DELAY_MS);
	}
}

void setLEDs (uint8_t x) {
    if( x & (1<<0) )        // Bit 0 in x gesetzt ?
		LED_PORT_BIT0_2 |= (1<<LED_BIT0); 	// ja: LED on
    else
		LED_PORT_BIT0_2 &= ~(1<<LED_BIT0); // nein: LED off	

    if( x & (1<<1) )        // Bit 1 in x gesetzt ?
		LED_PORT_BIT0_2 |= (1<<LED_BIT1); 	// ja: LED on
    else
		LED_PORT_BIT0_2 &= ~(1<<LED_BIT1); // nein: LED off

    if( x & (1<<2) )        // Bit 2 in x gesetzt ?
		LED_PORT_BIT0_2 |= (1<<LED_BIT2); 	// ja: LED on
    else
		LED_PORT_BIT0_2 &= ~(1<<LED_BIT2); // nein: LED off

    if( x & (1<<3) )        // Bit 4 in x gesetzt ?
		LED_PORT_BIT3_7 |= (1<<LED_BIT3); 	// ja: LED on
    else
		LED_PORT_BIT3_7 &= ~(1<<LED_BIT3); // nein: LED off

    if( x & (1<<4) )        // Bit 4 in x gesetzt ?
		LED_PORT_BIT3_7 |= (1<<LED_BIT4); 	// ja: LED on
    else
		LED_PORT_BIT3_7 &= ~(1<<LED_BIT4); // nein: LED off

    if( x & (1<<5) )        // Bit 5 in x gesetzt ?
		LED_PORT_BIT3_7 |= (1<<LED_BIT5); 	// ja: LED on
    else
		LED_PORT_BIT3_7 &= ~(1<<LED_BIT5); // nein: LED off

    if( x & (1<<6) )        // Bit 6 in x gesetzt ?
		LED_PORT_BIT3_7 |= (1<<LED_BIT6); 	// ja: LED on
    else
		LED_PORT_BIT3_7 &= ~(1<<LED_BIT6); // nein: LED off

    if( x & (1<<7) )        // Bit 7 in x gesetzt ?
		LED_PORT_BIT3_7 |= (1<<LED_BIT7); 	// ja: LED on
    else
		LED_PORT_BIT3_7 &= ~(1<<LED_BIT7); // nein: LED off
}
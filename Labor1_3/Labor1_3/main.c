/*
 * Labor1_3.c
 *
 * Kurzzeitwecker
 * Author : Steven
 */ 

#include "defines.h"

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdbool.h>

//Timer1 Settings
#define PRESCALE 8 //Use clk/8 Prescaler
#define CLOCK_CYCLES 0x10000-((F_CPU/PRESCALE)/1000) //One Millisec for Timer1

enum STATE {
	NORMAL=0,
	IN_SET_MODE=1,
	COUNTDOWN=2,
	ALARM=3
};


volatile bool beepOn;
volatile unsigned long milliseconds; //Holds Milliseconds of Runtime
volatile unsigned long previous;  //Used in ISR
volatile unsigned long seconds; //Holds Seconds of Runtime

enum STATE state=NORMAL;

unsigned long millis() {
	return milliseconds;
}

unsigned long sec() {
	return seconds;
}

void setLEDs (uint8_t x) {
	if( x & (1<<0) )        // Bit 0 in x gesetzt ?
	LED_PORT_BIT0_2 |= (1<<LED_BIT0); 	// ja: LED on
	else
	LED_PORT_BIT0_2 &= ~(1<<LED_BIT0); // nein: LED off

	if( x & (1<<1) )        // Bit 1 in x gesetzt ?
	LED_PORT_BIT0_2 |= (1<<LED_BIT1); 	// ja: LED on
	else
	LED_PORT_BIT0_2 &= ~(1<<LED_BIT1); // nein: LED off

	if( x & (1<<2) )        // Bit 2 in x gesetzt ?
	LED_PORT_BIT0_2 |= (1<<LED_BIT2); 	// ja: LED on
	else
	LED_PORT_BIT0_2 &= ~(1<<LED_BIT2); // nein: LED off

	if( x & (1<<3) )        // Bit 4 in x gesetzt ?
	LED_PORT_BIT3_7 |= (1<<LED_BIT3); 	// ja: LED on
	else
	LED_PORT_BIT3_7 &= ~(1<<LED_BIT3); // nein: LED off

	if( x & (1<<4) )        // Bit 4 in x gesetzt ?
	LED_PORT_BIT3_7 |= (1<<LED_BIT4); 	// ja: LED on
	else
	LED_PORT_BIT3_7 &= ~(1<<LED_BIT4); // nein: LED off

	if( x & (1<<5) )        // Bit 5 in x gesetzt ?
	LED_PORT_BIT3_7 |= (1<<LED_BIT5); 	// ja: LED on
	else
	LED_PORT_BIT3_7 &= ~(1<<LED_BIT5); // nein: LED off

	if( x & (1<<6) )        // Bit 6 in x gesetzt ?
	LED_PORT_BIT3_7 |= (1<<LED_BIT6); 	// ja: LED on
	else
	LED_PORT_BIT3_7 &= ~(1<<LED_BIT6); // nein: LED off

	if( x & (1<<7) )        // Bit 7 in x gesetzt ?
	LED_PORT_BIT3_7 |= (1<<LED_BIT7); 	// ja: LED on
	else
	LED_PORT_BIT3_7 &= ~(1<<LED_BIT7); // nein: LED off
}

void timer_Init() {
	//Setup Timer
	TCCR1B  = BIT(CS11); //clk/8
	TCNT1 = CLOCK_CYCLES;
	TIMSK = BIT(TOIE1); //Interrupt Bit
}


volatile unsigned long previousBeep; //Runtime
void beep() {
	beepOn=true;
}


volatile unsigned long previousTaster; //Runtime
bool taster_pressed(uint8_t taster) {
	if(!(TASTER_PIN & BIT(taster)) && (millis()-previousTaster>200)) {
		previousTaster=millis();
		beep();
		return true;
	}

	return false;
}

//Timer1 Interrupt Service Routine
ISR(TIMER1_OVF_vect)
{
	++milliseconds;
	if(milliseconds-previous>1000) {
		previous=milliseconds;
		++seconds;
	}
	TCNT1 = CLOCK_CYCLES;
}

int main(void)
{
	TASTER_DDR	&= ~BIT(TASTER1) & ~BIT(TASTER2);
	TASTER_PORT |= BIT(TASTER1) | BIT(TASTER2);
	
	LED_DDR_BIT0_2 |= BIT(LED_BIT0) | BIT(LED_BIT1) | BIT(LED_BIT2);
	LED_DDR_BIT3_7 |= BIT(LED_BIT3) | BIT(LED_BIT4) | BIT(LED_BIT5) | BIT(LED_BIT6) | BIT(LED_BIT7);

	BUZZER_DDR |= BIT(BUZZER_BIT);
	BUZZER_PORT &= ~BIT(BUZZER_BIT);


	timer_Init();
	sei(); //millis() replacement
	uint8_t timeSet=0;
	unsigned long previousTimeSet=0; //Runtime
	unsigned long previousBeep=0; //Runtime
	unsigned long previousCountdown=0; //Runtime
	bool ledState=true;
	unsigned long beepPulseCount=0;

	while (true)
	{
		if(beepOn && (millis()-previousBeep>=1)) {
			previousBeep=millis();
			beepPulseCount++;
			BUZZER_PORT ^=BIT(BUZZER_BIT);
		}
		else if(beepPulseCount>15 && state!=ALARM) {
			beepOn=false;
			beepPulseCount=0;
		}
		else if(beepPulseCount>=16384 && state==ALARM) {
			beepOn=false;
			timeSet=0;
			beepPulseCount=0;
			state=NORMAL;
		}

		if(state==NORMAL) {
			setLEDs(seconds); //Shows normal time

			if(taster_pressed(TASTER2)) {
				state=IN_SET_MODE;
			}
		}
		
		else if(state==IN_SET_MODE) {
		//Blink LED's
			if((ledState==true) && (millis()-previousTimeSet>100)) {
				ledState=false;
				previousTimeSet=millis();
				setLEDs(0x00);
			}
			else if ((ledState==false) && (millis()-previousTimeSet>100)){
				ledState=true;
				previousTimeSet=millis();
				setLEDs(timeSet);
			}

			if(taster_pressed(TASTER1)) {
				++timeSet;
			}

			if(taster_pressed(TASTER2) && timeSet>0) {
				--timeSet;
			}

			if((!(TASTER_PIN & BIT(TASTER1))) && (!(TASTER_PIN & BIT(TASTER2))) && timeSet>0) {
				state=COUNTDOWN;
				_delay_ms(500); //Optimize this
			}
		}

		else if(state==COUNTDOWN) {
			if(sec()-previousCountdown>=1) {
				previousCountdown=sec();
				--timeSet;
			}
			setLEDs(timeSet);

			if(timeSet==0) {
				beep();
				state=ALARM;
			}

			if(taster_pressed(TASTER1)) { //Pause
				state=IN_SET_MODE;
			}
			if(taster_pressed(TASTER2)) { //Stop
				timeSet=0;
				state=NORMAL;
			}
		}

		else if(state==ALARM) {
			if((ledState==true) && (millis()-previousTimeSet>100)) {
				ledState=false;
				previousTimeSet=millis();
				setLEDs(0x00);
			}
			else if ((ledState==false) && (millis()-previousTimeSet>100)){
				ledState=true;
				previousTimeSet=millis();
				setLEDs(0xFF);		
			}

			if(taster_pressed(TASTER1)) {
				state=NORMAL;
			}
		}
	}
}
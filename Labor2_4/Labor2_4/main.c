/*
 * Labor2_4.c
 *
 * LED Dimmen mit PWM, ADC und Interrupt + Sleep
 * Author : Steven
 */ 

#include "defines.h"

#include <avr/io.h>
#include <stdbool.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/sleep.h>

const uint8_t pwmtable_8D[32] =
{
	0, 1, 2, 2, 2, 3, 3, 4, 5, 6, 7, 8, 10, 11, 13, 16, 19, 23,
	27, 32, 38, 45, 54, 64, 76, 91, 108, 128, 152, 181, 215, 255
};

void setupADC() {
	//Setup ADC
	ADMUX=	(1<<MUX2) | (1<<MUX0) |	//Select PC5 ADC5
			(1<<REFS1) |(0<<REFS0) | (1<<ADLAR);		//Internal 2,56V Reference
	
	ADCSRA=	(1<<ADEN) |								//Enabled, Single Conversion Mode
			(1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0) |	//Full Prescaler for 93,75KHz conversion (12Mhz/128)=93,75KHz
			(1<<ADSC) |								//Start Conversion
			(1<<ADIE);								//Interrupts Enabled

	(void) ADCW; //Dummy read
}

void setupTimer1() {
	TCCR1B |= (1<<CS11) | (1<<CS10);	//Timer enabled, Overflow (Normal) Mode, Prescaler clk/64
	TIMSK |= (1<<TOIE1);				//Enable Overflow Interrupt
	TCNT1 = 46786;						//100ms
}

void setupTimer2() {
	TCCR2=	(1<<WGM21) | (1<<WGM20) |			//Fast PWM
			(1<<COM21) | (0<<COM20) |			//Clear OC2 on Compare Match,set OC2 at BOTTOM (non-inverting mode)
			(0<<CS22) | (1<<CS21) | (1<<CS20);	//Prescaler Clk/1024 =11,71875KHz 
}

ISR(ADC_vect)
{
	//ADCSRA &= 
	OCR2 = pwmtable_8D[ADCH>>3];	// set PWM according to ADCH
}

ISR(TIMER1_OVF_vect) {
	ADCSRA |= 1<<ADSC;	//Request Conversion
	TCNT1 = 46786;		//100ms
}

int main(void)
{
	LED_DDR_BIT0_2 |= BIT(LED_BIT0);
	LED_PORT_BIT0_2 &= ~BIT(LED_BIT0);
	
	setupADC();
	setupTimer2();
	setupTimer1();
	sei();

	while (1)
	{
		set_sleep_mode(SLEEP_MODE_IDLE);
		sleep_mode();
	}
}


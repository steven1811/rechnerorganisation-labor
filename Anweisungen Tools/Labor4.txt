Labor 4: Caches und Speicherzugriffszeiten
==========================================

Ziel ist es, die Größe der Cache Speicher in einem PC
zu ermitteln sowie die Zyklenzahl, die für Speicherzugriffe
bei Cache Hit/Miss benötigt wird.
Fertigen Sie von den Versuchen ein Laborprotokoll an.

Benötigt wird ein PC mit Linux, z.B. Labor WH G 505.

Besser ist es jedoch, wenn Sie einen eigenen PC/Laptop
nehmen, damit am Ende unterschiedliche CPU und 
Speicherkonfigurationen verglichen werden können.

Benötigt wird die Benchmarking Software lmbench-3.0-a9
von Larry McVoy und Carl Staelin.
Die Quellen finden Sie hier: 
https://sourceforge.net/projects/lmbench/

Laden Sie die Quellen herunter, packen ggf. 
das Archiv aus und öffnen eine Shell. Gehen
Sie in das top-level Verzeichnis lmbench-3.0-a9
und geben Sie "make" ein.
Es sollte ein neues Unterverzeichnis "bin" entstehen
und darunter ein architekturabhängiges Verzeichnis
in dem alle Benchmark Tools gebaut werden.

Für die Tests wird nur ein Tool benötigt: lat_mem_rd

Rufen Sie das Tool zunächst ohne Parameter auf, 
es erscheint eine kurze Hilfe zur Benutzung.

Das Tool soll nur auf einem Prozessor laufen und 
der Cache vorher schon "aufgewärmt" werden, also 
wählen Sie zunächst die Optionen -P1 und -W1.
Die Anzahl -N der Wiederholungen sollten Sie 
zu Beginn klein wählen -N 1 damit das Prog. 
schnell durchläuft, für die endgültigen Messungen 
aber größer (mindestens 10), damit Ausreißer 
emliminiert werden können. 

Die eigentlichen Parameter len und stride können Sie am 
Besten verstehen, wenn Sie sich die Quellen in src/lat_mem_rd.c 
anschauen: Die Laufzeit der Funktion benchmark_loads 
wird gemessen. Die innerste Schleife von benchmark_loads 
besteht aus 100 Zeilen "p = (char **)*p;" die durch das 
Makro HUNDRED erzeugt werden.

Der neue Wert von Pointer p wird aus der Speicherzelle 
geladen auf die der aktuelle Wert von Pointer p zeigt. 
Was man nicht sofort sieht: Der Speicher wurde vorab
so initialisiert, dass p innerhalb eines Array der 
Länge len immer einen konstanten Offset (stride) 
weiter"springt".

Also ähnlich einer Schleife: 
  for(i=0; i<len i+= stride) 
    lese(A[i]);

Die etwas umständliche Konstruktion des Abspeichern 
der Pointer innerhalb des Arrays dient dazu, dass 
der Compiler keine Chance hat, den Code zu 
optimieren, und die CPU auch keine Chance hat, 
Ladezugriffe zu parallelisieren: Die nächste Adresse 
p ist erst nach dem Laden von *p bekannt :-) 

Durch die 100fache Wiederholung wird der Einfluss 
des "loop-overhead" der Schleife stark verringert.

Kontrollieren Sie den Assemblercode , indem Sie 
das Binary disassemblieren und dort die Funktion 
benchmark_loads betrachten:

  objdump -d bin/x86_64-linux-gnu/lat_mem_rd | more +/benchmark_loads

Übernehmen Sie die disassemblierte Funktion 
auszugsweise in Ihr Protokoll.


Jetzt sollen die Zugriffszeiten gemessen werden, 
die ein Ladebefehl benötigt und zwar in Abhängigkeit 
von der Gesamtlänge des Arrays und vom Stride.

Das Tool unternimmt einige Anstrengungen um 
näherungsweise die durchschnittliche Zahl der 
CPU Taktzyklen pro Ladebefehl zu ermitteln und
auszugeben, was einem Prog. im User Mode eines 
Multi-Tasking OS nur näherungsweise gelingen kann.
Achten Sie daraauf, dass während der Messungen 
das System möglichst unbelastet bleibt.

Überlegen Sie sich, wie sich die Zugriffszeit 
tendenziell verhalten wird bei festem stride==1 
(auf jedes Element wird zugegriffen), wenn 
a) len sehr klein ist (< L1 Größe)
b) len größer als die L1 Größe aber kleiner als die L2 Größe ist ?
c ) usw.. für L3 und ggf. L4 ?

Notieren Sie Ihre Vermutungen im Protokoll.



Führen Sie jetzt den ersten Versuch aus. 
Dabei wählen Sie stride 16 und eine maximale 
Länge (len) von 1024 MB, also:

  lat_mem_rd -W1 -P 1 -N 10 1024 16

(stride == 1 gibt oft keine sinnvollen Werte, mehr dazu gleich.)

Die erste Spalte des Outputs enthält die wachsende 
Länge des untersuchten Speicherbereiches in MB und 
die zweite Spalte die ermittelte mittlere Dauer 
der Ladezugriffe in CPU Zyklen.

Erzeugen Sie aus dem Output mit Excel oder LibreOffice Calc 
ein XY (Punkte) Diagramm. Wählen Sie für beide Achsen eine 
logarithmische Teilung der Achsen und beschriften Sie diese.
Übernehmen Sie das Diagramm in ihr Protokoll.

Sie sollten Unterschiede in den Zugriffszeiten 
sehen, die treppenartig ansteigen. 
Wodurch entstehen die Treppenstufen?



Wenn der stride klein ist, sind die Unterschiede 
zwischen den Zugriffszeiten recht gering. Erhöhen 
Sie stride nun auf 1024 und messen Sie erneut:

  lat_mem_rd -W1 -P 1 -N 10 1024 1024
  
Fügen Sie die zweite Messkurve in das XY Diagramm hinzu.
Übernehmen Sie das Diagramm in ihr Protokoll.

Für das Protokoll: Was ist jetzt anders? 
Wie erklären Sie sich den Effekt ?



Wiederholen Sie die Messungen mit einem größeren 
N und geben als letzte Parameter für stride gleich 
mehrere Werte an, nämlich alle Zweierpotenzen von 
16 bis 1024.



Schlagen Sie auf www.cpu-world.com die Informationen 
zu der auf ihrem System eingesetzten CPU und den Caches nach.
Sie können, falls installiert, unter Linux auch Tools wie
lscpu, lshw, lstopo benutzen oder schauen im sysfs unter
/sys/devices/system/cpu/... nach.

Nehmen Sie die ermittelten Daten mit in das Protokoll auf.
Ermitteln Sie auch die Anzahl der CPU Kerne und ob Ihre
CPU Hyperthreading unterstützt, was die Ausführung von
2 Threads auf einem Kern ermöglicht, so dass z.B. auf
einer Quad-Core CPU 8 Threads gleichzeitig laufen können.


Erhöhen Sie nun die Parallelität, d.h. mehrere Instanzen
des Speicherzugriffstests laufen gleichzeitig. Linux wird
diese gleichmäßig auf die CPU Kerne aufteilen, so dass 
mehrere Ladebefehle tatsächlich gleichzeitig  
ausgeführt werden. Geben Sie, bei typischen Werten 
für len und stride, wachsende Werte für den -P Parameter 
ein und vergleichen die Zugriffszeiten wieder in einem 
Diagramm.  

Fragen: Sind die Datenpfade des Speichersystems 
so performant, dass alle CPU Kerne mit voller 
Geschwindigkeit arbeiten können ohne sich 
gegenseitig zu behindern?
Was passiert wenn Sie mehr Instanzen starten 
als es CPU Kerne (inkl. Hyperthreading) gibt? 
Begründen Sie kurz.

Mit Hilfe des Befehles taskset kann man einen 
Linux Prozess an einen einzelnee CPU Kern binden, 
z.B.

  taskset 1 lmbench ... 

Wiederholen Sie die Parallelitätstests 
für -P 1 und -P 2 mit "taskset 1" und 
erklären Sie die Ergebnisse.

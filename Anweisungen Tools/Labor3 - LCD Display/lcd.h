/*
 * lcd.h
 *
 * Created: 11.12.2015 17:46:57
 *  Author: Frank
 */ 


#ifndef LCD_H_
#define LCD_H_

// Initialisierung der IO Pins
// der SPI Kommunikation
// des Displays 
void LCD_init();

// ausgabe eines Zeichens
void LCD_char(char c);

// Ausgabe einer Zeichenkette wie "Hello"
void LCD_string(char *str);

// setzt die Schreibposition, oben links ist (0,0)
int LCD_cursor(unsigned int line, unsigned int col);

#endif /* LCD_H_ */

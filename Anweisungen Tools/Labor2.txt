Dimmbare LED
============
Dieselben Voraussetzungen wie bei Projekt1.
Weiterhin benötigen Sie das Datenblatt des
µC, das Sie auf dem Dozentenserver oder 
im Internet finden. ("ATmega8/L datasheet")
sowie einen Reglerknopf für den blauen 
drehbaren Einstellwiderstand.

Ziel:
Die LED an PB3 soll auf verschiedene 
Arten in ihrer Helligkeit gesteuert 
werden. Dabei werden Hardwareeinheiten 
des µC benutzt: 
- ADC (Analog/Digital Converter), 
- PWM (Pulse Width Modulation), 
- Timer Interrupt

Digitalen Eingang und LED
=========================
Schliessen Sie den Ausgang des blauen 
Einstellwiderstandes ("Trimmer") an 
den Anschluss PC5 des µC an. Am Pin PB3 
ist eine LED angeschlossen. Schreiben Sie
ein Programm, das diese LED einschaltet, 
wenn an PC5 ein high Pegel anliegt und
sie ausschaltet, wenn an PC5 ein 
low Pegel anliegt. Am Input PC5 soll kein 
pull-up angeschlossen sein, da der Trimmer 
bereits die nötigen Pegel zwischen 0 und 5V liefert.

Drehen Sie den Trimmer so lange, bis 
die LED angeht und bis sie wieder ausgeht.
Der Übergang wird recht plötzlich und undefiniert 
(bei ca. 2,43 V) stattfinden, weil der digitale 
Eingang keine Zwischenzustände ausser high und 
low annimmt. Er enthält dazu eine spezielle 
Schaltung, einen "Schmitt-Trigger", vgl. 
"Figure 22. General Digital I/O" im Datenblatt.

Im Weiteren soll die Helligkeit der LED in Abhängigkeit
von der Einstellung des Trimmers kontinuierlich 
einstellbar gemacht werden.

Analoge Spannung messen und Messwert ausgeben
=============================================
Pin PC5 hat zusätzlich die spezielle Funktion 
ADC5 (Analog-Digital Konverter, Kanal 5).
Diese Funktion soll nun genutzt werden, 
um den Pegel an PC5 zu digitalisieren und auszugeben. 
Zur Ausgabe verwenden Sie alle 8 LED und
die Funktion setLEDs(uint8_t x) aus dem
vorhergehenden Projekt. Dei Initialisierung
aller LED Ausgängen lagern Sie zweckmäßigerweise
in eine Funktion void initLEDs() aus.

Schlagen Sie im Datenblatt des ATmega8 nach,
was dort zum Analog-Digital Konverter beschrieben 
ist, sehen Sie sich insbesondere an:
"Figure 90. Analog to Digital Converter Block Schematic Operation"
Dort entnehmen Sie die Namen (Abkürzungen) der Register
ADC MULTIPLEXER SELECT und ADC CTRL. & STATUS REGISTER.
Diese müssen mit geeigneten Werten beschrieben werden:

Setzen Sie im Mux Register die Bits REFS0 und ADLAR (Ergebnis linksbündig),
löschen Sie das Bit REFS1 und setzen die unteren 4 Bit (MUX)
auf 5, die Nummer des Eingangskanals. Die 01 Kombination der
beiden REFS Bits stellt die Referenzspannung die zum Vergleich 
verwendet wird auf AVCC (analoge Versorgungsspannung).

Stellen Sie Ctrl.&Status Register den Prescaler auf einen 
passenden Wert (unterste 3 Bit). Sehen Sie sich dazu den
Text um "Figure 91. ADC Prescaler" an, der einen Vorschlag
für einen geeigneten Frequenzbereich macht. Beachten Sie
dabei, dass ihr Board mit einem CPU Takt von 12 MHz läuft.

Setzen Sie im Ctrl.&Status Register die Bits ADEN und ADFR
Dadurch wird der AD Wandler eingeschaltet (enabled) und der
"free running" Mode eingestellt, d.h. nach jeder Messung wird
sofort wieder eine nächste Messung gestartet. 
Die allererste Messung muss nun mit ADCSRA |= (1<<ADSC); 
gestartet werden.

Jetzt müssen Sie im Programm in einer while Schleife nur 
noch den aktuellen Messwert auslesen, an setLEDs übergeben
und dann eine kleine Weile warten, z.B. mit _delay_ms(10);

Da Sie oben "linksbündig" eingestellt haben, enthält 
das Register ADCH die oberen 8 Bit des Messergebnisses.
Die restlichen (unteren) Bits stehen in ADCL und können
für unsere Zwecke ignoriert werden.

Übergeben Sie den Wert des ADCH Registers an setLEDs.
Bauen, laden und starten Sie das Programm. Wenn Sie jetzt
am Trimmer drehen, sollten Sie je nach Stellung des Trimmers
binäre Ausgaben im Wertebereich 0..255 bekommen.

LED an PB3 dimmen
=================
Die binäre Ausgabe diente nur als Test dafür, das die 
Messungen korrekt erfolgen. Der ausgelesenen Wert in ADCH
soll nun verwendet werden, um die Helligkeit der LED zu variieren.

Der ATmega8 hat keine analogen Ausgänge, man kann also keine
Spannung bzw. keinen Strom variieren um die Helligkeit
der LED anzupassen. Stattdessen soll die LED in kurzen
Zeiträumen ein und wieder ausgeschaltet ("gepulst") 
werden. Etwa so:

#define F_CPU 12000000
#include <util/delay.h>
...
	while(1)
	{
		// Messergebnis in ADCH
		uint8_t x = ADCH;
		LedPB3on();
		for( int i=0; i<255; i++)
		{
			if(i==x)
				LedPB3off();
			_delay_ms(1);
		}
	}

Jeder Schleifendurchlauf wird jetzt ca. 256 ms dauern, wenn
man die wenigen CPU Taktzyklen der Wertzuweisung und 
der LED on/off Funktionen vernachlässigt.
Aber das Verhältnis der "on Zeit" zur "off Zeit" variiert 
und damit die wahrgenommenen Helligkeit der LED. 
Frage: mit welcher Frequenz blinkt die LED ?

Lassen Sie die LED nun 1000 Mal schneller blinken,
indem Sie _delay_ms durch _delay_us ersetzen.
Das Blinken kann der Mensch nun nicht mehr wahrnehmen.

Look-up Table für eine linearisierte Helligkeit
=========================================
Die Helligkeit soll nun in 32 Stufen eingestellt 
werden, so dass die Unterschiede zw. zwei Stufen
etwa gleich stark wahrgenommen werden. Nehmen Sie
also nur die obersten 5 Bit von ADCH und verwenden 
Sie diese als Index in eine Look-up Tabelle:

// Quelle: http://www.mikrocontroller.net/articles/LED-Fading
const uint8_t pwmtable_8D[32] =
{
	0, 1, 2, 2, 2, 3, 3, 4, 5, 6, 7, 8, 10, 11, 13, 16, 19, 23,
	27, 32, 38, 45, 54, 64, 76, 91, 108, 128, 152, 181, 215, 255
};

Bauen und Testen Sie erneut ihr Programm.

PWM
===
Ein großer Nachteil des obigen Codes besteht darin, dass
der µC die ganze Zeit mit Warten beschäftigt ist und dabei 
ja auch extra Strom verbraucht, nur um eine LED zu dimmen. 
Das Ein- und Ausschalten kann man aber einer Hardwareinheit 
im µC übertragen, der PWM (Pulse-width-modulation) Funktion 
eines Timers.
Diese tut dasselbe wie oben, schaltet einen Ausgang
bei konstanter Frequenz in einem einstellbaren Verhältnis
von "on" und "off" Zeit. Die Breite ("width") des 
High Impulses ("Pulse", "duty-cycle") ist einstellbar (modulierbar).

Man könnte durch ein nachgeschaltetes Integrationsglied 
(RC Kombi) am Ausgang eine Spannung quasi analog einstellen. 
Aufgrund der Trägkeit des menschlichen Sehens 
ist das hier aber gar nicht notwendig.

Benutzen Sie als PWM Einheit den "8-bit Timer/Counter2".
Dafür muss das Timer/Counter Control Register TCCR2 
wie folgt programmiert werden:

Die unteren 3 Bits von TCCR2 auf den Wert 1 einstellen, 
vgl. "Table 46. Clock Select Bit Description", 
die COM Bits auf die Kombination (1<<COM21) | (0<<COM20)
(vgl. "Table 45. Compare Output Mode")
und die WGM Bits auf die Kombination (1<<WGM21) | (1<<WGM20)
("Table 42. Waveform Generation Mode Bit Description")

Wenn Sie jetzt das Register OCR2 auf einen Wert zw. 
0 und 255 setzen, wird sich die Helligkeit der LED 
entsprechend des eingestellten Wertes ändern.
Setzen Sie in der while Schleife nur noch OCR2=ADCH und
schauen Sie, wie sich die Helligkeit der LED verändert.
Frage: Warum wird gerade die an PB3 angeschlossene LED gedimmt?
Antworttipp: Schauen Sie sich die COM2 bits im Datenblatt an.

Die CPU im µC ist jetzt zwar nicht mehr mit
dem "Software PWM" belastet, wartet aber immer 
noch in der Endlosschleife.

Verwenden des ADC Interruptes
=============================
Der ADC kann sich mit einem Interrupt bemerkbar 
machen, sobald er eine Messung beendet hat. 
Schreiben Sie einen Interrut Handler, der 
die PWM (über den Table Lookup) jedesmal
nachstellt, wenn der ADC einen neuen Messwert 
ermittelt hat:

#include <avr/interrupt.h>

ISR(ADC_vect)
{
	OCR2 = pwmtable_8D[ADCH>>3];	// set PWM according to ADCH
	//ADCSRA |= 1<<ADSC;		// start next conversion, not needed when free running mode is on
}

main()
{
	...
	sei();	// global enable interupts
	ADCSRA |= (1<<ADIE); // ADC completion interrupt enable
	...
	while(1)
		...
}

siehe auch: http://www.avr-tutorials.com/adc/utilizing-avr-adc-interrupt-feature

Die CPU kann nun für andere Dinge benutzt 
werden bzw. in einen Schlafmodus versetzt,
um Strom zu sparen.

Der ADC Interrupt kommt sehr oft, viel öfter als notwendig.
Schreiben Sie abschließend einen Timer Interupt, 
der nur alle ca. 100 ms eine einzelne ADC Messung auslöst. 
Benutzen Sie dazu den 8-Bit Timer/Counter0 oder
den 16-Bit Timer/Counter1
und lesen Sie im Datenblatt sowie anderen Quellen 
Ihrer Wahl nach, wie Sie einen periodischen Timer
Interrupt realisieren können.

Durch den Timer Interrupt sinkt auch die Rate der ADC Interrupts.
Frage: mit welcher Frequenz erfolgen jetzt die Messungen?

Die mainloop ist nun weitgehend überflüssig geworden, 
weil sämtliche Aktivitäten interuptgesteuert ablaufen. 

Die CPU soll daher in der Hauptschleife in einem Schlafmode 
versetzt werden um Strom zu sparen. Der Schlafmode muss
in der Hauptschleife immer wieder aktiviert werden, weil 
die CPU durch einen Interrupt ständig wieder aufgeweckt
wird, um die ISR ausführen zu können.

Benutzen Sie dazu die in <avr/sleep.h> bereitgestellten 
Funktionen. Lesen Sie 
- im Datenblatt unter "Power Management and Sleep Modes",
unter http://www.mikrocontroller.net/articles/Sleep_Mode und
unter http://www.atmel.com/webdoc/AVRLibcReferenceManual/ch20.html
nach, welcher Schlafmodus für Ihre Anwendung in Frage kommt.

Nehmen Sie ihre sleep Mode Implementierung in Betrieb und demonstrieren
dessen Funktionstüchtigkeit dadurch, dass Sie an den verschiedenen
Stellen der mainloop zu Testzwecken LEDs ein- bzw. ausschalten.
Je nachdem ob die CPU länger schläft oder länger "wach ist", 
werden die LED heller oder dunkler erscheinen (PWM).

Beachten Sie folgende Eigentümlichkeit der ADC: Sobald die CPU
in einen Sleep Mode geht, startet *automatisch* eine ADC Messung
wenn ADC enabled ist.
Damit die Hauptschleife also nicht dauernd ADC Messungen auslöst,
sollten Sie das ADEN (ADC enable) Bit benutzen und die ADC
an geeigneten Stellen im Code aktivieren und deaktivieren.
Hintergrundinfo: die ADC kann die Spannung präziser messen, 
wenn die CPU nicht durch ihre digitalen Signale auf dem Chip 
stört, daher dieser Automatismus.

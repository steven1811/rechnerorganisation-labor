Labor 5: Virtueller Speicher
============================

Benötigt wird ein Linux PC wie z.B. 
in Labor WH G 505 (Ubuntu 14 ISO 64 Bit)
sollte aber auch auf ähnlichen Systemen 
gehen, der C Code ist portabel, 
das Verhalten hängt aber auch vom 
Betriebssystem ab. 

In der Beispiellösung war memory 
overcommitment aktiv (mehr dazu 
unten) d.h. das Kommando 

  sysctl vm.overcommit_memory

liefert die Ausgabe

  vm.overcommit_memory = 0

Ihr System sollte während aller Tests 
möglichst unbelastet sein, d.h. lassen
Sie keine anderen Anwendungen... laufen.

Protokollieren Sie bitte alle durchgeführten 
Tests und Ausgaben auf ihrem System.

Schreiben sie ein kleines C Programm
virtual_memory.c das
a) eine globale Variable vom Typ int hat
b) in der main() Funktion eine lokale 
   Variable vom Typ int definiert.
c) in der main() Funktion einen Pointer
   vom Typ char* definiert.
d) keine andere Funktion als main() enthält

Das Programm soll in mehreren Phasen
laufen, nach jeder Phase soll es auf
eine Eingabe wie folgt warten:

  printf("... press ENTER to continue\n");
  getchar();

Die notwendigen Header inkludieren Sie 
bitte selbstständig.

Finden Sie nun heraus, wieviel Hauptspeicher
in ihrem System installiert ist, Beispielsweise
in einer Shell durch den Aufruf von 

  free -mh

Ausgabe:

             total       used       free     shared    buffers     cached
Mem:          7,7G       3,4G       4,4G       760M       372M       2,4G
-/+ buffers/cache:       579M       7,1G
Swap:           0B         0B         0B

Interessant ist die erste Zahl: 7,7G, aufgerundet: 8 GiB Hauptspeicher

(Falls Sie root Rechte haben, können Sie mit
  dmidecode --type 17 
oder
  lshw -C memory
detailierte Infos über den RAM erhalten.)


Phase 1 (malloc):
-----------------
Reservieren Sie mit malloc 75% der 
Hauptspeichergröße und weisen die 
Adresse dem char* zu.

Geben Sie mit printf aus:
- die Adresse der globalen Variablen
- die Adresse der lokalen Variablen
- die Adresse der main Funktion
- den Wert des mit malloc erzeugten Pointers
  (==Adresse des Beginns mit malloc allozieren Speichers auf dem Heap)
- die Adresse des Endes des mit malloc allozieren Speichers auf dem Heap
  (vom Programm ausrechnen lassen).

Benutzen Sie in der printf Ausgabe die Formatierung %16p

Phase 2 (memset):
-----------------
Rufen Sie memset auf und füllen den kompletten
allozieren Heapspeicherbereich mit dem Wert 0x55. 

Phase 3 (exit):
---------------
Das Programm wird beendet ohne den 
allozierten Heapspeicher freizugeben.


Das Programm soll von der Kommandozeile
aus wie folgt übersetzt werden:

  gcc virtual_memory.c 



Versuch 1:
----------
Öffnen Sie eine Shell und führen Sie dort free -m aus.
Öffnen Sie eine zweite Shell und starten Sie in dieser 
ihr Programm (Phase 1). 
Notieren Sie sich die ausgegebenen Adressen 
und fertigen Sie eine Skizze des Speicherlayouts 
an. 

Wie viele Bits (ausser führenden Nullen) haben diese Adressen?
Wie groß sind die größten Adressen (in Giga/Tera,... Byte)?
Ist so viel physikalischer RAM überhaupt vorhanden?
 
Rufen Sie free -mh erneut auf und notieren, wie 
sich used und free verändert haben.
Starten Sie jetzt Phase 2 ihres Programmes und 
betrachten Sie used und free erneut. Was ist passiert?
Beenden Sie ihr Programm und starten free -mh erneut.
Wie haben sich used und free verändert?
Ist der Speicher wieder freigegeben, wenn ja: von wem?




Versuch 2:
----------
Lassen Sie das Programm mehrmals 
hintereinander laufen. 
Notieren Sie die angezeigten Adressen.
Welche Adressen sind in immer wieder gleich, 
welche unterschiedlich?

Sehen Sie sich den Wert des char* an
und dort insbesondere die untersten Bits. 
Was stellen Sie fest? Stellen Sie einen 
Zusammenhang zur Seitengröße (page size) 
von 4 kiB her.



Versuch 3:
----------
Lassen Sie wieder zwei Instanzen des Programmes 
gleichzeitig (in zwei shells) laufen:

Starten Sie eine Instanz Ihres Programmes 
bis zu Phase 2.

Starten Sie jun eine zweite Instanz in
einer weiteren Shell. Was passiert?
Erklären Sie das Verhalten.



Versuch 4:
----------
Lassen Sie wieder zwei Instanzen des Programmes 
gleichzeitig (in zwei shells) laufen:

Starten Sie zwei Instanzen Ihres Programmes,
aber diesmal nur bis zur Phase 1 in 
separaten Shells.

Notieren Sie wieder alle Adressen.
Welche Adressen sind in beiden Instanzen 
gleich, welche unterschiedlich?

Wenn zwei Adressen gleich sind: könnte man  
durch Schreiben und Lesen auf diese Adresse 
zwischen den beiden Programm Instanzen 
kommunizieren? Begründen Sie kurz.

Beobachten Sie wieder mit free -mh den Speicher.

Gehen Sie nun bei der ersten Instanz zu Phase 2.
Erkennen Sie Unterschiede zum Versuch 3? Welche?

Sobald memset der ersten Instanz fertig ist, 
starten Sie Phase 2 der zweiten Instanz. 

Was passiert?
(Achtung: das ist eine Linux Besonderheit 
und keine Eigenschaft der Sprache C oder
der Hardware)

Ist es denkbar, dass beide Programme 
bis zu Phase 3 kommen? Begründen Sie.




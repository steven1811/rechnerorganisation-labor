/*
 * Labor3_1.c
 *
 * Assembler 1 - Zuweisung 42
 * Author : Steven
 */ 

#include "defines.h"
#include <avr/io.h>

extern int func();

int main(void)
{
	int i = func();
	return i;
}
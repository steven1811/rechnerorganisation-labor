/*
 * defines.h
 *
 * Defines USB AVR Stick
 *  Author: Steven
 */ 

  /*
 Stecker zeigt Links von Oberer Ansicht betrachtet

 PB0: Taster1
 PB1: Taster2

 PC4: LED Links (MSB)
 PC0: F�nfte LED
 PB5: Sechste LED
 PB3: LED Rechts (LSB)

 PC5: Poti
 PD6: Buzzer
 */

#ifndef DEFINES_H_
#define DEFINES_H_

//Clock Speed
#define F_CPU 12000000

//Makro
#define BIT(i) (1<<(i))

//Taster
#define TASTER_PORT PORTB
#define TASTER_DDR DDRB
#define TASTER_PIN PINB

#define TASTER1 PB0
#define TASTER2 PB1


//LED's
#define LED_PORT_BIT0_2 PORTB
#define LED_PORT_BIT3_7 PORTC

#define LED_DDR_BIT0_2 DDRB
#define LED_DDR_BIT3_7 DDRC

#define LED_BIT0 PB3
#define LED_BIT1 PB4
#define LED_BIT2 PB5
#define LED_BIT3 PC0
#define LED_BIT4 PC1
#define LED_BIT5 PC2
#define LED_BIT6 PC3
#define LED_BIT7 PC4


//Buzzer
#define BUZZER_PORT PORTD
#define BUZZER_DDR	DDRD
#define BUZZER_BIT PD6


//Poti
#define POTI_PORT PORTC
#define POTI_DDR PORTC
#define POTI_BIT PC5


//Display
#define DB7 7
#define DB6 6
#define DB5 5
#define DB4 4
#define DB3 3
#define DB2 2
#define DB1 1
#define DB0 0

#define LCD_SPI_DDR DDRB
#define LCD_SPI_PORT PORTB

#define LCD_CONTROL_DDR DDRC
#define LCD_CONTROL_PORT PORTC

#define LCD_CLK PB5
#define LCD_SI PB3
#define LCD_CE PC1
#define LCD_RS PC0
#define LCD_SS PB2

#endif /* DEFINES_H_ */
/*
 * lcd.c
 *
 * LCD Driver
 *  Author: Steven
 */ 

 #include "lcd.h"

  void transmit_data(uint8_t data) {
	  SPDR = data; //Load Data to register
	  while(!(SPSR & (1<<SPIF))); //Wait till transmission is complete
	  (void) SPDR;
  }

  void send_command(uint8_t cmd) {
		LCD_CONTROL_PORT&= ~((1<<LCD_RS) | (1<<LCD_CE));
		transmit_data(cmd);
		LCD_CONTROL_PORT|= (1<<LCD_CE) | (1<<LCD_RS);

		//Toggle
	  	LCD_CONTROL_PORT&=~(1<<LCD_RS);

		_delay_us(26.3); //Safety delay for execution time
  }

  void write_data(uint8_t data) {
	LCD_CONTROL_PORT&= ~(1<<LCD_CE);
	LCD_CONTROL_PORT|=(1<<LCD_RS);
	transmit_data(data);
	LCD_CONTROL_PORT|= (1<<LCD_CE);

	LCD_CONTROL_PORT&=~(1<<LCD_RS);
	LCD_CONTROL_PORT|=(1<<LCD_RS);
	_delay_us(26.3); //Safety delay for execution time
  }

  //Serial Only
   void LCD_function_set() {
	   send_command((1<<DB5) | (1<<DB4) | (_lcd->lines<<DB2) | (_lcd->useDoubleHeightFont<<DB2) | (_lcd->instructionSet << DB0));
   }

   //What does this do?
   void LCD_set_bias() {
		send_command((1<<DB4));
   }

   void LCD_contrast_set() {
		send_command((1<<DB6) | (1<<DB5) | (1<<DB4) | (_lcd->contrast << DB0));
   }

   //What does this do?
   void LCD_power_control() {
		send_command((1<<DB6) | (1<<DB4));
   }

   //What does this do?
   void LCD_follower_control() {
		send_command((1<<DB6) | (1<<DB4));
   }

   void LCD_diplay_control() {
	   send_command((1<<DB3) | (_lcd->displayOn<<DB2) | (_lcd->cursorOn<<DB1) | (_lcd->cursorPosOn << DB0));
   }

 void LCD_init(const LCD_Display *lcd) {
	 _lcd=lcd;
	 //Init DDR Register
	 LCD_SPI_DDR |= (1<<LCD_SI) | (1<<LCD_CLK) | (1<<LCD_SS);
	 LCD_CONTROL_DDR |= (1<<LCD_RS) | (1<<LCD_CE);

	 //CE Low
	 LCD_CONTROL_PORT &= ~((1<<LCD_CE) | (1<<LCD_RS));

	 //RS LOW
	 LCD_CONTROL_PORT &= ~(1<<LCD_RS);

	 //SS High
	 LCD_SPI_PORT |= (1<<LCD_SS);

	 //Initialise SPI
	 SPCR =	(1<<SPE) |	//Enable SPI
			(1<<MSTR)|	//Set as Master
			(0<<SPR0) | (0<<SPR1) | (1<<CPHA) | (1<<CPOL) | (0<<SPI2X);	//clk/2

	 _delay_ms(250);	//Wait >40ms After VDD stable

	 send_command(0x39);
	 send_command(0x1C);
	 send_command(0x52);
	 send_command(0x69);
	 send_command(0x74);
	 send_command(0x38);
	 send_command(0x0F);
	 send_command(0x01);
	 send_command(0x06);	
	 
	  _delay_ms(250);

	 /*LCD_function_set();		//Call Function Set
	 _delay_us(30);				//Wait time >26.3us
	 LCD_function_set();		//Call Function set Again
	 _delay_us(30);				//Wait time >26.3us
	 LCD_set_bias();			//????????
	 _delay_us(30);				//Wait time >26.3us
	 LCD_contrast_set();		//Set Contrast
	 _delay_us(30);				//Wait time >26.3us
	 LCD_power_control();		//????????
	 _delay_us(30);				//Wait time >26.3us
	 LCD_follower_control();	//????????
	 _delay_ms(250);			//Wait time >200ms (for power stable)
	 LCD_diplay_control();		//Enable Display
	 _delay_us(30);				//Wait time >26.3us*/
	 
	 //Initialization end
 }

 // ausgabe eines Zeichens
 void LCD_char(char c) {
	write_data(c);
 }

 // Ausgabe einer Zeichenkette wie "Hello"
 void LCD_string(char *str) {
	for(unsigned long i=0;i!=strlen(str);i++) {
		LCD_char(str[i]);	
	}
 }

 // setzt die Schreibposition, oben links ist (0,0)
 int LCD_cursor(unsigned int line, unsigned int col) {
	//Set DDRAM
	uint8_t address=(line*0x40)+col;
	address&=(0b01111111);
	send_command((1<<DB7) | address);
 }
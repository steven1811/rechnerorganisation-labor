/*
 * lcd.h
 *
 * Created: 11.12.2015 17:46:57
 *  Author: Frank
 */ 


#ifndef LCD_H_
#define LCD_H_

#include "defines.h"
#include <avr/io.h>
#include <stdbool.h>
#include <util/delay.h>
#include <string.h>

//Commands
 
typedef enum  {
	 LCD_DISPLAY_LINES_1= false,
	 LCD_DISPLAY_LINES_2= true
} LCD_DISPLAY_LINES;

typedef enum  {
	LCD_INSTRUCTION_SET_NORMAL=0b00,
	LCD_INSTRUCTION_SET_EXTENDED_1=0b1,
	LCD_INSTRUCTION_SET_EXTENDED_2=0b10,
	LCD_INSTRUCTION_SET_UNUSED=0b11
} LCD_INSTRUCTION_SET;

typedef struct  {
	 LCD_DISPLAY_LINES lines;
	 LCD_INSTRUCTION_SET instructionSet;
	 bool useDoubleHeightFont;
	 uint8_t contrast; //4 Bit
	 bool displayOn;
	 bool cursorOn;
	 bool cursorPosOn;
} LCD_Display;

const LCD_Display *_lcd;

// Initialisierung der IO Pins
// der SPI Kommunikation
// des Displays 
void LCD_init(const LCD_Display *lcd);

// ausgabe eines Zeichens
void LCD_char(char c);

// Ausgabe einer Zeichenkette wie "Hello"
void LCD_string(char *str);

// setzt die Schreibposition, oben links ist (0,0)
int LCD_cursor(unsigned int line, unsigned int col);

#endif /* LCD_H_ */

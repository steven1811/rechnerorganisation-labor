/*
 * Labor3_3.c
 *
 * DOGM 162 LCD Display Stoppuhr
 * Author: Steven
 */ 
#include "defines.h"
#include "lcd.h"
#include <avr/io.h>
#include <string.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <avr/sleep.h>

#define PRESCALE1 8
#define PRESCALE2 32
#define CLOCK_CYCLES1 ((F_CPU/PRESCALE1)/1000) //One Millisec for Timer1
#define CLOCK_CYCLES2 0xFF //Rougly 23Hz with clk/1024 prescaler for screen refresh

#define cyclesBuzzer 100;
extern volatile uint16_t ms;
extern volatile uint8_t ss;
extern volatile uint8_t mm;
extern volatile uint8_t hh;

extern void tick();
extern int func();

volatile char buffer[50];
volatile uint16_t taster1Count;
volatile bool taster1Hold;

typedef enum {
	PAUSED,
	COUNTING,
} STATE;


volatile STATE currentState=PAUSED; //Initial State
volatile bool buzzerOn=false;
volatile uint8_t buzzerCycles;

LCD_Display lcd1;

ISR(TIMER1_COMPA_vect)
{
	if(currentState==COUNTING) {
		tick();
	}

	if(buzzerOn) {
		BUZZER_PORT^=(1<<BUZZER_BIT);
		buzzerCycles--;
	}

	if(buzzerOn && buzzerCycles==0) {
		buzzerCycles=cyclesBuzzer;
		buzzerOn=false;
	}
	OCR1A= CLOCK_CYCLES1;
}

ISR(TIMER2_COMP_vect) {
	sprintf (buffer, "%d:%d:%d:%d", hh, mm, ss, ms);
	LCD_string(buffer);
	LCD_cursor(0,0);	
	OCR2= CLOCK_CYCLES2;

	//Poll Key
	if(!(TASTER_PIN & (1<<TASTER1))) {
		taster1Hold=true;
		taster1Count++;
	}
	else {
		taster1Hold=false; //Released
	}

	//Check For Long Press Key
	if(taster1Count>=50 && taster1Hold==false) {
		buzzerOn=true;
		taster1Count=0;
		ms=0;
		ss=0;
		mm=0;
		hh=0;
		currentState==PAUSED;
	}

	if(currentState==PAUSED) {
		if(taster1Count>=2 && taster1Hold==false) { //On Short Press start Counting
			buzzerOn=true;
			taster1Count=0;
			currentState=COUNTING;	
		}
	}

	if(currentState==COUNTING) {
		if(taster1Count>=2 && taster1Hold==false) { //On Short Press freeze Count
			buzzerOn=true;
			taster1Count=0;
			currentState=PAUSED;
		}		
	}

	if(taster1Count<=50 && taster1Hold==false) { //Reset dismissed Key
		taster1Count=0;
	}
}

void beep() {
	
}

void setupTimer1() {
	//Setup Timer
	TCCR1B  =	(1<<CS11) |					//clk/8
				(1<<WGM12);					//CTC Mode
	TCNT1 = 0;
	OCR1A= CLOCK_CYCLES1;
	TIMSK |= (1 << OCIE1A); //Enable COMPA Interrupt
}

//Refreshes Display and checks input
void setupTimer2() {
	TCCR2 =		(1<<CS21) | (1<<CS20) | (1<<CS22) | //clk/1024
				(1<<WGM21);				//CTC Mode
	TCNT2= 0;
	OCR2= CLOCK_CYCLES2;
	TIMSK |= (1<< OCIE2);
}

int main(void)
{
	TASTER_DDR &= ~(1<<TASTER1 | 1<<TASTER2);
	TASTER_PORT |= (1<<TASTER1) | (1<<TASTER2);

	BUZZER_DDR |= (1<<BUZZER_BIT);
	BUZZER_PORT &= ~(1<<BUZZER_PORT);
	buzzerCycles=cyclesBuzzer;

	setupTimer1();
	setupTimer2();

	lcd1.instructionSet=LCD_INSTRUCTION_SET_NORMAL;
	lcd1.lines=LCD_DISPLAY_LINES_2;
	lcd1.useDoubleHeightFont=false;
	lcd1.contrast=0b1000;
	lcd1.cursorOn=true;
	lcd1.displayOn=true;
	lcd1.cursorPosOn=true;

    LCD_init(&lcd1);
	LCD_cursor(0,0);

	sei();

    while (1) 
    {
		set_sleep_mode(SLEEP_MODE_IDLE);
		sleep_mode();
    }
}


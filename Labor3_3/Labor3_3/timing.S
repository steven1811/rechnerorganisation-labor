
/*
 * timing.S
 *
 *	Assembler2 - Timing
 *  Author: Steven
 */ 
#include <avr/io.h>

#define _MILLI_COUNT	1000
#define _MIN_COUNT		60
#define _HOUR_COUNT		60
#define _DAY_COUNT		24

 	.section .data

	.global hh
	hh: .byte 0

	.global mm
	mm: .byte 0

	.global ss
	ss: .byte 0

	.global ms
	ms: .word 0

	.section .text
	.global tick
	.global func

	func:
		ldi r24, 42
		ret

	tick:
		lds r24, ms			; untere 8 Bit laden

		lds r25, ms+1		; obere 8-Bit laden
		adiw r24, 1			; Add to word register 1 
		ldi r26, lo8(_MILLI_COUNT)	; Load lower nibble of Dez. 1000
		ldi r27, hi8(_MILLI_COUNT)	; Load upper nibble of Dez. 1000
		cp  r24, r26		; Compare lower nibble with Lower nibble
		cpc r25, r27		; Compare upper nibble with upper nibble
		sts ms  , r24		; Save lower Nibble
		sts ms+1, r25		; Save upper Nibble
		brge tick_ss		; branch wenn 1000
		ret
	tick_ss:
		ldi r24,0;			; Reset ms
		sts ms, r24			; Save ms

		lds r24, ss			; untere 8 Bit laden
		ldi r25, _MIN_COUNT	; Load 60	
		ldi r26, 1			; Load 1
		add r24, r26		; Add 1
		cp r24, r25			; Compare ss+1 with 60
		sts ss, r24			; Store ss+1
		brge tick_mm		; Inc Minute
		ret
	tick_mm:
		ldi r24,0;			; Reset ss
		sts ss, r24			; Save ss
		
		lds r24, mm			; untere 8 Bit laden
		ldi r25, _HOUR_COUNT; Load 60					
							; Load 1 Already happened
		add r24, r26		; Add 1
		cp r24, r25			; Compare
		sts mm, r24			; Store mm+1
		brge tick_hm		; Inc Hour
		ret
	tick_hm:
		ldi r24,0;			; Reset mm
		sts mm, r24			; Save mm
		
		lds r24, hh			; untere 8 Bit laden
		ldi r25, _DAY_COUNT	; Load 24					
							; Load 1 Already happened
		add r24, r26		; Add 1
		cp r24, r25			; Compare
		sts hh, r24			; Store mm+1
		brge tick_day		; Inc Hour
		ret		
	tick_day:
		ldi r24,0;			; Reset hh
		sts hh, r24			; Save hh		
		ret					; End

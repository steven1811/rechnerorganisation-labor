/*
 * Labor2_3.c
 *
 * LED Dimmen mit LUT und PWM
 * Author : Steven
 */ 

#include "defines.h"

#include <avr/io.h>
#include <stdbool.h>
#include <avr/interrupt.h>
#include <util/delay.h>

void setupADC() {
	//Setup ADC
	ADMUX= (1<<MUX2) | (1<<MUX0)  | (1<<REFS1) |(1<<REFS0); //Select PC5 ADC5, Internal 2,56V Reference
	
	ADCSRA=(1<<ADEN) | (1<<ADFR) | (1<<ADPS0) | (1<<ADSC); //Enabled, Free running, Full Prescaler for 6MHz conversion (12Mhz/2)=6MHz, Start Conversion

	(void) ADCW; //Dummy read
}

void setupTimer2() {
	TCCR2= (1<<WGM21) | (1<<WGM20) | (1<<COM21) | (0<<COM20) | (1<<CS22) | (1<<CS21) | (1<<CS20); //Fast PWM, Clear OC2 on Compare Match, set OC2 at BOTTOM (non-inverting mode), Prescaler Clk/1024
}

void LEDBit0off() {
	LED_PORT_BIT0_2 &= ~BIT(LED_BIT0);
}

void LEDBit0on() {
	LED_PORT_BIT0_2 |= BIT(LED_BIT0);
}

int main(void)
{
	LED_DDR_BIT0_2 |= BIT(LED_BIT0);
	LED_PORT_BIT0_2 &= ~BIT(LED_BIT0);
	
	setupADC();
	setupTimer2();

	while (1)
	{
		OCR2=ADCH;
	}
}


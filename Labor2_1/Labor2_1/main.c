/*
 * Labor2_1.c
 *
 * Dimmbare LED
 * Author : Steven
 */ 

#include "defines.h"

#include <avr/io.h>
#include <stdbool.h>
#include <avr/interrupt.h>
#include <util/delay.h>

void setupADC() {
	//Setup ADC
	ADMUX= (1<<MUX2) | (1<<MUX0) | (1<<REFS1) |(1<<REFS0); //Select PC5 ADC5, Internal 2,56V Reference
	
	ADCSRA=(1<<ADEN) | (1<<ADFR) | (1<<ADPS0) | (1<<ADSC); //Enabled, Free running, Full Prescaler for 6MHz conversion (12Mhz/2)=6MHz, Start Conversion

	(void) ADCW; //Dummy read
}

void LEDBit0off() {
	LED_PORT_BIT0_2 &= ~BIT(LED_BIT0);
}

void LEDBit0on() {
	LED_PORT_BIT0_2 |= BIT(LED_BIT0);
}
int main(void)
{
	LED_DDR_BIT0_2 |= BIT(LED_BIT0);
	LED_PORT_BIT0_2 &= ~BIT(LED_BIT0);

	setupADC();

    while (1) 
    {
		uint8_t x = ADCH;
		LEDBit0on();

		for( int i=0; i<255; i++)
		{
			if(i==x)
			LEDBit0off();
			_delay_us(1);
		}
    }
}


/*
 * Labor_1_1.c
 *
 * Elektronischer Kurzzeitwecker
 * Author : Steven
 */ 

#include <avr/io.h>

#define BIT(i) (1<<(i))
int main(void)
{
	DDRB &= ~BIT(PB0); // PB0 input
	PORTB |= BIT(PB0); // PB0 with pull-up
	DDRC |= BIT(PC4); // PC4 output
	PORTC |= BIT(PC4); // PC4 high (LED on)
	while (1)
	{
		if (PINB & BIT(PB0)) // Taster offen
		PORTC |= BIT(PC4); // high (LED on)
		else // Taster geschlossen
		PORTC &= ~BIT(PC4); // low (LED off)
	}
}